﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public float speed;
    int count = 0;

    public Text countText;

    public Text winText;

    public Rigidbody groundRB;

    bool dead = false;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {
        if (dead == false && transform.position.y <= -5) 
        {
            dead = true;
            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 2f);
        }
	}

    void RestartLevel()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Vertical");
        float moveVertical = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        //rb.AddForce(movement * Time.deltaTime * speed);

        groundRB.AddTorque(movement * Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;

            SetCountText();

            Destroy(other.gameObject);
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 12)
        {
            winText.text = "You Win";
        }
    }
}
