﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;

    Vector3 offset;

    public GameObject ground;

	// Use this for initialization
	void Start () {
        offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (player !=  null)
            transform.position = player.transform.position + offset;
        //Vector3 groundRotation = ground.transform.rotation.eulerAngles;
       // transform.rotation = Quaternion.Euler(new Vector3(groundRotation.x + 60f, groundRotation.y, groundRotation.z));
		
	}
}
